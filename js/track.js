window.track = (function () {
    const text = String.raw`
                                              /--------------------------------------------------------------------------------\                      
                                      /-------+--------------------------------------------------------------------------------+-\                    
   /----------------------------------+-------+-----------------\     /--------\                                               | |                    
   |                                  |       |/----------------+-----+--\     |                                               | |     /------------\ 
   |                         /--------+-------++-------------<--+-----+--+-----+-----------------------------\  /--------------+-+-----+-\          | 
   |  /----------------------+--------+-------++----------\     |     | /+-----+----\          /-------------+-\|       /------+-+---\ | |          | 
   |  |                      |   /----+-------++----------+-----+-----+-++-----+----+----------+-------------+-++-------+------+-+\  | | |          | 
   |  |                      |   |/---+-------++----------+-----+\    | ||     | /--+----------+------------\| ||       |      | ||  | | |          | 
   |  |                      |   ||   |       ||          |     ||    | ||     | |  |     /----+------------++-++-------+------+-++--+-+\|          | 
   |  |              /-------+---++---+-------++----------+-----++----+-++----\| |  |     |  /-+------------++-++-------+------+-++--+-+++---\      | 
   |  |              | /-----+---++---+-------++----------+-----++----+-++-\  || |  |     |  | |            || ||       |      | ||  | |||   |      | 
   |  |              | | /---+---++---+-------++----------+-----++----+\|| |  || |/-+-----+--+-+------------++-++-------+------+-++--+-+++\  |      | 
   |  |              | | |   |  /++---+-------++----------+-----++----++++-+--++-++-+-----+--+-+------------++-++-------+------+-++--+\||||  |      | 
   |  |              | | |   |  |||   |       ||    /-----+-----++----++++-+--++-++-+-----+--+-+------------++-++-------+------+-++--++++++--+-----\| 
   |  |              | | |   |  |||   |       ||    |/----+-----++--\ |||| |  || || |     |  | |            || |\-------+------+-++--++++/|  |     || 
   \--+-->-----------+-+-+---+--+++---+-------++----++----+-----/|  | ||\+-+<-++-++-/     |  | |            || |        |      | ||  |||| |  |     || 
      |      /-------+-+-+---+\ |||   |       ||    ||    |      |  | || | |  || ||       |/-+-+------------++-+--------+------+-++--++++-+--+-\   || 
      |      |       | | |   || ||| /-+-------++----++----+------+--+-++-+-+--++-++-------++-+-+------------++-+--------+------+\||  |||| |  | |   || 
      |      |       | | |   || ||| | |       ||    ||    |      |  | || | |  || ||       || | |            || |        |      ||||  |||| |  | |   || 
      |   /--+-------+-+-+---++-+++-+-+--\    ||    ||    |      |  | || | |  || ||       || | |            || |        |      ||||  |||| |  | |   || 
      |   |  |       | |/+---++-+++-+-+--+----++----++----+------+--+-++\| |  || ||       || | |            || |      /-+------++++-\|||| |  | |   || 
      |   |  |       | ||| /-++-+++-+-+--+----++----++----+------+-\| |||| |  || ||       || | |            || |      | |      |||| ||||| |  | |   || 
      |   |  |       | ||| | || ||| | |  |    |^    ||    |      | || |||| |  || ||       || | | /----------++-+\     | |      |||| ||||| |  | |   || 
      |   | /+-------+-+++-+-++-+++-+-+--+----++----++----+------+-++-++++-+--++-++-------++-+-+\|          || ||     | |      |||| ||||| |  | |   || 
      |   | ||       | ||| | || ||| | |  |    ||    ||    |      |/++-++++-+--++-++-------++-+-+++----------++-++-----+-+------++++-+++++-+--+\|   || 
      |   |/++-------+-+++-+-++-+++-+-+--+----++----++----+------++++-++++-+--++-++-------++-+-+++----------++-++-----+\|      |||| ||||| |  |||   || 
      |   ||||       | ||| | || ||| | \--+----++----++----+------++++-++++>+--++-++-------++-+-+++----------++-++-----+++------++/| ||||| |  |||   || 
      |   ||||       | ||| | ||/+++-+----+----++----++----+------++++-++++-+--++-++-------++-+-+++----------++-++-----+++--\   || | ||||| |  |||   || 
      |   ||||  /----+-+++-+-++++++-+----+----++----++--\ |      |||| |||| |  || ||       || | |||          || ||     |||  |   || | ||||| |  |||   || 
      |   ||||  |    | ||| | |||||| |    |    ||    ||  | |      |||| |||| |  || ||       || | |||          || ||     ||\--+---++-+-+/||| |  |||   || 
      |   ||||  |    | ||| | |||||| |    | /--++----++--+-+------++++-++++-+--++-++-------++-+-+++----------++-++--\  ||   |   || | | ||| |  |||   || 
      |   ||||  |    | ||| | |||||| |    | |  ||    ||  |/+------++++-++++-+--++-++-------++-+-+++----------++-++--+--++---+---++-+-+-+++-+--+++---++\
      |   ||||  |    | ||| | |||||| |    | |  ||   /++--+++------++++-++++-+--++-++-------++-+-+++----------++-++--+--++---+---++-+-+-+++-+-\|||   |||
      |   |||\--+----+-+++-+-+/|||| |    | |  ||   |\+--+++------++++-++++-+--++-++-------++-+-+++----------++-++--+--++---+---++-+-+-+++-+-++++---/||
      v   |||   |    | ||| | | |||| |    | |  ||   | | /+++------++++-++++-+--++-++-------++-+-+++----------++-++--+--++---+---++-+-+-+++-+-++++---\||
  /---+---+++---+--\ | ||| | | |||| |    | |  ||   | | ||||      |||| |||| |  || ||     /-++-+-+++----------++-++--+-\||   |   || | | ||| | ||||   |||
  |   |   |||   |  | | ||| | | |||| |    | |  \+---+-+-++++------++++-++++-+--++-++-----+-++-+-+++----------++-++--+-+++---+---/| | | ||| | ||||   |||
  |   |   |||   |  | | ||| | | |||| |    | |   |   | |/++++------++++-++++-+--++-++-----+-++-+-+++-----\    || ||  | |||   |    | | | ||| | ||||   |||
  |   |   |||   |  | | ||| | | |||| |  /-+-+---+---+-++++++------++++-++++-+--++-++-----+-++\| |||     |    || ||  | |||   |    | | | |\+-+-++++---+/|
  |   |   |||   |  | | ||| | | |||| |  | | |  /+---+-++++++------++++-++++-+--++-++-----+-++++-+++-----+----++-++--+-+++---+----+-+-+-+-+-+\||||   | |
  |   |   |||   |  | | ||| | | |||| |  |/+-+--++---+-++++++------++++-++++-+--++-++-----+-++++-+++-----+----++\||  | |||   |    | | | | | ||||||   | |
  |   |   |||   |  | | ||| | | |||| |  ||| |  ||   | ||||||      |||| |||| |  || ||     | |||| \++-----+----+++/|  | |||   |    | | | | | ||||||   | |
  |  /+---+++---+--+-+-+++-+-+-++++-+--+++-+--++---+-++++++------++++-++++-+--++-++\    | ||||  ||     |    ||| |  | |||   |    | | | | | ||||||   | |
  |  ||   |||   |  | | ||| | |/++++-+--+++-+--++---+-++++++------++++-++++-+--++-+++----+-++++--++-----+--\ ||| |  | |||   |    | | | | | ||||||   | |
  |  ||   \++---+--+-+-+++-+-++++++-+--++/ |  ||   | ||||||      |||| |||| |  || |||    | ||||  ||     |  | ||| |  | |||   |    | | | | | ||||||   | |
  |  ||    ||   |  | | ||| | |||||| |  ||  |  ||   | ||||||      |||| |||| |  || \++----+-++++--++-----+--+-/|| |  | |||   |    | | | | | ||||||   | |
  |  ||    ||   |  | | ||| | |||||| | /++--+--++---+-++++++------++++-++++-+--++--++----+-++++--++-----+--+--++-+--+-+++--\|    | | | | | ||||||   | |
  |  ||    || /-+--+-+-+++-+-++++++-+-+++--+--++---+-++++++------++++-++++-+--++--++----+-++++--++-----+--+\ || |  | |||  ||    | | | | | ||||||   | |
  | /++----++-+-+--+-+-+++-+-++++++-+-+++--+--++---+-++++++--\   |||| |||| |  ||  ||    | |\++--++-----+--++-++-+--+-+++--++----+-+-+-+-+-+++++/   | |
  | |||    || | |  | | ||| | |||||| | |||  |  ||   | ||||||  |   |||| |||| |  ||  \+----+-+-++--++-----+--++-++-+--+-+++--++----+-+-+-+-+-/||||    | |
  | |||    || | |  | | ||| | |||||| | |||  |  ||   | ||||||  |   |||| |||| |  ||   |    | | ||  ||     |  || || v  | |||  ||    | | | | |  ||||    | |
  | |||    || | |  | | ||| | |||||| | |||  |  ||   | \+++++--+---+++/ |||| |  ||   |    | | ||  |\-----+--++-++-/  | |||  ||    | | | | |  ||||    | |
  \-+++----++-+-+--/ | ||| | |||||| | |||  |  ||   |  |||||  |/--+++--++++-+--++--\|    | | ||  |      |/-++-++----+-+++--++----+-+-+-+-+--++++-\  | |
    |||    || | | /--+-+++-+-++++++-+-+++--+--++---+--+++++-\||  |||  |||| |  ||  ||    | | ||  |      || |v ||    | |||  ||    | | | | |  |||| |  | |
    |||    || | | |  \-+++-+-++++++-+-+++--+--++---+--+++++-+++--+++--++++-+--/|/-++----+-+-++--+------++-++-++----+-+++--++-\  | | | | |  |||| |  | |
    |||    || | | |    ||| | \+++++-+-+++--+--++---+--+++++-+++--+++--++++-+---++-++----+-+-++--+------++-++-/|    | |||  || |  | | | | |  |||| |  | |
    |||    || | | |    |||/+--+++++-+-+++--+--++---+--+++++-+++--+++--++++-+---++-++----+-+-++--+------++-++--+----+-+++--++-+--+-+-+\| v  |||| |  | |
    |||    || | | |    |||||  ||||| | |||  |  ||   |  ||||| |||  |||  |||| |   || ||    | | || /+------++-++--+----+-+++--++-+--+-+-+++-+--++++-+-\| |
    |||    || | | |    |||||  ||||| | |||  |  ||   |  ||||| |||  |||  |||| | /-++-++----+-+-++-++-----\|| ||  | /--+-+++--++-+--+\| ||| |  |||| | || |
  /-+++----++-+-+-+----+++++--+++++-+-+++--+--++---+\ ||||| |||  |||  |||| | | || ||    | | |\-++-----+++-++--+-+--+-+++--++-+--+++-+++-+--++/| | || |
  | |||    || | | |    |||||  ||||| | |||  |  ||   || ||||| |||  |||  |||| | | || || /--+-+-+--++-----+++-++--+-+--+-+++--++-+--+++-+++-+--++-+-+-++\|
/-+-+++----++-+-+-+----+++++--+++++-+-+++--+--++---++-+++++-+++--+++--++++\| | || || |  | | |  ||     ||| ||  | |  | |||  || |  ||| ||| |  || | | ||||
| | |||    || | | |    |\+++--+++++-+-+++--+--++---++-+++++-+++--+++--++/||| | || || |  | | |  ||     ||| ||  | |  | |||  || |  ||| ||| |  || | | ||||
|/+-+++----++\| | |    | |||  ||||| | |||  |  ||   || ||||| |||  |||  ||/+++-+-++-++-+--+-+-+--++-----+++-++--+-+--+-+++-\|| |  ||| ||| |  || | | ||||
||| |||   /++++-+-+----+-+++--+++++-+-+++\ |  ||/--++-+++++-+++--+++--++++++-+-++-++-+--+\| |  ||     ||| ||  | |  | ||| ||| |/-+++-+++-+\ || | | ||||
||| |||   ||||| | |    | |||  ||||| | |||| |  |||  || ||||| |||  |||  |||||| | || || |  ||\-+--++-----+++-++--+-+--+-+++-+++-++-+++-+++-/| || | | ||||
||| |||   ||||| | |    | |||  ||||| | |||| |  |||  || ||||| |||  |||  |||||| | || || |  ||  |  ||     ||| ||  | |  | ||| ||| || ||| |||  | || | | ||||
||| |||   ||||| | |    | |||  ||||| | |||| |  |||  || ||||| |||  |||  \+++++-+-/| || |  ||  |  ||   /-+++-++--+-+--+-+++-+++-++-+++\|||  | || | | ||||
||| |||   ||||| | |    | ||\--+++++-+-++++-+--+++--++-+++++-+++--++/   ||||| |  | || |  ||  |  ||   | ||| ||  | |  | ||| ||| || |||||||  | || | | ||||
||| |||  /+++++-+-+----+-++---+++++-+-++++-+\ |||  || |\+++-+++--++----+++++-+--+-++-+--++--+--++---+-+++-++--+-+--+-+++-+++-++-+++++++--+-++-+-+-+/||
||| |||  |||||| | |    | ||   ||||| | |||| || |||  || | ||| |||  ||    |||||/+--+-++-+--++--+--++--\| ||| ||  | |  | ||| ||| || |||||||  | || | | | ||
||| |||  |||||| | |    | ||   ||||| | |||| ||/+++--++-+-+++-+++--++----+++++++--+\|| \--++--+--++--++-+++-++--+-+--+-+++-+++-++-+++++++--+-++-+-+-+-/|
||| |||  |||||| | |    | || /-+++++-+-++++-++++++--++-+-+++-+++--++----+++++++--++++----++--+--++--++-+++-++--+-+--+\||| ||| || |||||||  | || | | |  |
||| |||  |||||| | |    | || | ||||| | |||| ||||||  || | ||| |||  ||    |||||||  |||| /--++--+--++--++-+++-++\ | |  ||||| ||| || |||||||  | || | | |  |
||| |||  |||||| | |    | || | ||||| | |||| ||||||  || | ||| |||  ||    |||||||  |||| |  ||  |  ||  || ||| ||| | |  ||||| ||| || |||||||  | || | | |  |
||| |||  |||||| | |    | || | ||||| | |||| ||||||  || | ||| |||  ||    |||||||  |||| |  ||  |  ||  || ||| ||| | | /+++++-+++-++-+++++++--+-++-+\| |  |
||| |||  |||||| | |    | || | ||||| | |||| |||||\--++-+-+++-+++--++----+++++++--++++-+--+/  |  ||  || ||| ||| | |/++++++-+++-++\|||||||  | || ||| |  |
||| |||  |||||| | |    | || | ||||| | |||| |||||   || | ||| |||  |\----+++++++--++++-+--+---+--++--++-+++-+++-+-++++++++-+++-++++++++++--+-++-/|| |  |
||| |||  |||||| | |/---+-++-+-+++++-+-++++-+++++---++-+-+++-+++--+----\|||||||  |||| |  |   |  ||  || ||| ||| | |||||||| ||| ||||||||||  | ||  || |  |
||| |||  |||||| | ||   | || | ||||| | |||| |||||   || | ||| |||  |    ||||||||  |||| |  |   |  ||  || ||| ||| | |||||||| ||| ||||||||||  | ||  || |  |
||| |||  |||||| | ||   | \+-+-+++++-+-++++-+++++---++-+-+++-+++--+----+/||||||  ||||/+--+---+--++--++-+++-+++-+-++++++++\||| ||||||||||  |/++--++-+-\|
||| |||  |||||| | ||   |  | | ||||| | |||| |||||   || | ||| |||  |    | ||||||  ||||||  |   |  ||  || ||| ||| | |||||||||||| ||||||||||  ||||  || | ||
||| |||  |||||| | ||   |  | | ||||| | |||| |||||   || | ||| |||  |    | ||||||  ||||||  |   |  ||  || |||/+++-+-++++++++++++-++++++++++--++++\ || | ||
||| |||  |||||| | ||   |  | | ||||| \-++++-+++++---++-+-+++-+++--+----+-++++++--++++++--+---+--++--++-+++++++-+-++++++++++++-+++/||||||  ||||| || | ||
||| |||  |||||| | ||   |  | | ||||| /-++++-+++++-\ || | ||| |||  |    | ||||||  ||||||  |   |  ||  || ||||||| | |||||||||||| ||| ||||||  ||||| || | ||
||| |||  |||||| | ||   |  | | |||\+-+-++++-+++++-+-++-+-+++-+++--+----+-++++++--++++++--+---+--++--++-+++++++-+-++++++++++++-+++-+/||||  ||||| || | ||
||| |||  |||v|| | ||   |  | | ||| | | |||| ||||| | || | ||| |||  |    | ||||||  ||||||  |   |  ||  || ||||||| | |||||||||||| ||| | ||||  ||||| || | ||
||| |||  |||||| | ||   |  \-+-+++-+-+-++++-+++++-+-++-+-+++-+++--+----+-++++++--++++++--+---+--++--++-+++++++-+-++++++++++++-+++-+-++/|  ||||| || | ||
||| |||  |||||| \-++---+----+-+++-+-+-++++-+++++-+-++-+-/|| |||  |    | ||||||  ||||||  |/--+--++--++-+++++++-+-++++++++++++-+++-+-++-+\ ||||| || | ||
||| |||  ||\+++---++---+----+-+++-+-+-++++-+++++-+-++-+--++-+++--+----+-++++++--++++++--++--+--++--++-+++++++-+-+++++++/|||| ||| | || || ||||| || | ||
|||/+++--++-+++---++---+----+-+++-+-+-++++-+++++-+-++\|  || |||  |    |/++++++--++++++--++--+--++--++-+++++++-+-+++++++-++++\||| | || || ||||| || | ||
|||||||  || |||   ||   |    | ||| | | |||| ||||\-+-++++--++-+++--+----+++/||||  ||||||  ||  |  ||  || ||||||| | ||||||| |||||||| | || || ||||| || | ||
|||||||  || |||   ||   |    | ||\-+-+-++++-++++--+-++++--++-+++--+----+++-++++--++++++--++--+--++--++-+++++++-+-+++++++-++++++++-+-++-/| ||||| || | ||
|||||||  || |||/--++---+--\ | ||  \-+-++++-++++--+-++++--++-+++--/    ||| ||||  ||||||  ||  |  ||  || ||||||| | ||||||| |||||||| | ||  | ||||| || | ||
|||||||  || ||||  ||   |  | | ||    | |||| ||||  | ||||  \+-+++-------+++-++++--++++++--++--+--++--++-+++++++-+-+++++++-++++++++-+-++--+-+++++-++-+-+/
|||||||  || ||||  ||   |  | | ||    |/++++-++++--+-++++--\| ||\------<+++-++++--++/|||  ||  |  ||  || ||||||| | ||||||| |||||||| | ||  | ||||| || | | 
|||||||  || ||||  ||   |  | |/++----++++++-++++--+-++++--++-++--------+++-++++--++-+++--++--+--++--++-+++++++-+\||||||| |||||||| | ||  | ||||| || | | 
|||||||  || ||||  ||   |  | ||||    |||||| |||\--+-++++--++-++--------+++-++++--++-+++--++--+--++--++-+++++++-+++++++++-++++++++-+-++--+-++/|| || | | 
|||||||  || ||||  ||   |  | ||||    |||||| |||   | ||||  || ||        ||| ||||  || |||  ||  |  ||  || ||||||| ||||||||| |||||||| | ||  | || || || | | 
|||||||  || ||||  ||   |  |/++++----++++++-+++---+-++++--++-++---\    ||| ||||  || |||  |\--+--++--++-+++++++-+++++++++-++++++++-+-++--/ || || || | | 
|||||||  || ||||  ||   |  ||||||    ||\+++-+++---+-++++--++-++---+----+++-++++--++-+++--+---+--++--++-+++++++-+++++++++-++/||||| | ||    v| || || | | 
||||\++--++-++++--++---+--++++++----++-+++-+++---+-++++--++-+/   |    ||| |||| /++-+++--+---+--++--++-+++++++-+++++++++-++-+++++-+\||    || || || | | 
|||| ||  || ||||  ||   |  ||||||    || ||| |||   | ||||  ||/+----+----+++-++++-+++-+++--+---+--++--++-+++++++-+++++++++-++-+++++-++++>---++-++\|| | | 
|||| ||  || ||||  ||   |  ||\+++----++-+++-+++---+-++++--++++----+----+++-++++-+++-+++--+---+--++--++-+++++++-++++++/|| || ||||| ||||    || ||||| | | 
|||| ||  || ||||  ||  /+--++-+++----++-+++-+++---+-++++--++++--\ |    ||| ||\+-+++-+++--+---+--++--/| ||||||| |||||| || || ||||| ||||    || ||||| | | 
|||| ||  || ||\+--++--++--++-+++----++-+++-+++---+-++++--++++--+-+----+++-++-+-+++-+++--+---+--++---+-+++++/| |||||| || || ||||| ||||    || ||||| | | 
|||| ||  || || |  ||  ||  || |||   /++-+++-+++---+-++++--++++--+\|    ||| || | ||| |||  |   |  ||   | ||||| | ||||\+-++-++-+++++-++++----++-+++/| | | 
|||| ||  || || |  ||  ||  || ||\---+++-+++-+++---+-++++--++++--+++----+++-++-+-+++-+++--+---+--++---+-+++++-+-++++-+-++-++-/|||| ||||    || ||| | | | 
|||| ||  || || |  ||  ||  || ||    ||| ||| |||   | ||||/-++++--+++----+++-++-+-+++-+++--+---+-\||   | ||||| | |||| | || ||  |v|| ||||    || ||| | | | 
|||| ||  || || |  ||  ||  || ||    ||\-+++-+++---+-+++++-/|||  |||    ||| || | ||| |||  |   | |||   | ||||| | |||| | || ||/-++++-++++\   || ||| | | | 
|||| ||  || || |  ||  ||  || ||    ||  |\+-+++---+-+++++--+++--+++----+++-++-+-+++-+++--+---+-+++---+-+++++-+-/||| | || ||| |||| |||||   || ||| | | | 
|||| ||  || || |  ||  ||  || ||    ||  | | |||   | |||||  |||  |||    ||| || | ||| ||\--+---+-+++---+-+++++-/  ||| | || ||| |||| |||||  /++-+++-+-+\| 
|||| |\--++-++-+--++--++--++-++----++--+-+-+++---+-+++++--/||  |||    ||| || | ||| ||   |   | |||   | |||||    ||| | || ||| |||| |||||  ||| ||| | ||| 
|||| |   || ||/+--++--++--++-++----++--+-+-+++---+-+++++---++--+++---\||| || \-+++-++---+---+-+++---+-/||||    ||| | || ||| |||| |||||  ||| ||| | ||| 
|||| |   || ||||  ||  ||  || ||    |\--+-+-+++---/ |||||   \+--+++---++++-++---+++-++---+---+-+++---+--++++----+++-+-++-+++-++++-+++++--+++-++/ | ||| 
||\+-+---++-++++--++--++--++-++----+---+-+-+++-----+/|||    |  |||   |||| ||   \++-++---+---+-+++---+--++++----+++-+-++-+++-++++-+/|||  ||| ||  | ||| 
|| | |   || |||\--++--++--/| ||    \---+-+-+++-----+-+++----+--+/|   |||| ||    || ||   |   | |||   |  ||||    ||| | || ||| |||| | |||  ||| ||  | ||| 
|| |/+---++-+++---++--++---+-++--------+-+-+++-----+-+++----+--+-+---++++-++---\|| ||   |   | |||   |  ||||    ||| | || ||| |||| | |||  ||| ||  | ||| 
|| |||   || |||   ||  |\---+-++--------+-+-+++-----+-+++----+--+-+---++++-+/   ||| ||   |   | |||   |  ||||    ||| | || ||| |||| | |||  ||| ||  | ||| 
|| |||   || |||   ||/-+----+-++--------+-+-+++-----+-+++----+--+-+---++++-+----+++-++---+---+\|||   |  ||||    ||| | || ||| |||| | |||  ||| ||  | ||| 
|| \++---++-+++---+++-+----+-++--------+-+-+++-----+-/||    |  | |   |||| |    ||| ||   |   |||||   |  ||||   /+++-+-++-+++-++++-+-+++--+++-++-\| ||| 
\+--++---++-+++---+++-+----+-++--------+-+-+++-----+--++----+--+-+---++++-/    ||| ||   |   |||||   |  ||||   |||\-+-++-+++-+++/ | |||  ||| || || ||| 
 |  ||   |\-+++---+++-+----+-++--------+-/ |||     |  ||    |  | |   ||||      ||| ||   |   |||||   |  ||||   |||  | |\-+++-+++--+-+/|  ||| || || ||| 
 |  ||   |  |||   ||| |    | ||        |   |||     |  ||    |  | |   ||||      ||| ||   \---+++++---+--++++---+++--+-/  ||| |||  | | |  ||| || || ||| 
 |  ||   |  |||   ||| \----+-++--------+---+++-----+--++----+--/ |   ||||      ||| ||       |||||   |  ||||   |||  |    ||| |||  | | |  ||| || || ||| 
 |  ||   |  |||   |||      | ||        |   ||\-----+--++----+----+---++++------++/ ||       |||||   |  ||||   |||  |    ||| |||  | | |  ||| || || ||| 
 |  ||   |  |||   |||      | ||        |   \+------+--++----+----+---++++------++--++-------+++++---+--++++---+++--/    ||| |||  | | |  ||| || || ||| 
 |  ||   |  |||   |\+------+-++--------+----+------+--++----+----+---+/||      ||  ||       |||||   |  ||||   |||       ||| |||  | | |  ||| || || ||| 
 |  ||   |  |||   | |      | ||        |    |      |  \+----+----+---+-++----->++--++-------+++++---+--/|||   ||\-------+++-+++--/ | |  ||| || || ||| 
 |  ||   |  |||   \-+------+-++--------+----+--<---+---+----/    |   | ||      ||  ||       |||||   |   |||   ||        ||| |||    | |  ||| || || ||| 
 |  ||   |  \++-----+------+-++--------+----+------+---+---------+---+-++------++--++-------++++/   |   \++---++--------+++-+++----+-+--+++-++-+/ ||| 
 |/-++---+---++-----+------+-++--------+----+------+---+---------+---+-++-----\||  ||       ||||    |    ||   ||        ||\-+++----+-/  ||| || |  ||| 
 || ||   |   ||     |      | ||        |    |      |   \---------+---+-++-----+++--++-------++/|    |    ||   ||        ||  |||    |    ||| || |  ||| 
 || ||   |   ||     |      | ||        |    |      \-------------+---+-++-----+++--++-------++-+----+----++---++--------++--+++----+----+++-/| |  ||| 
 || ||   |   ||     |      | ||        \----+--------------------+---+-++-----+++--++-------/| |    |    ||   ||        ||  |||    |    |||  | |  ||| 
 || ||   |   ||     |      | ||             |                    |   | ||     |||  |\--------+-+----+----++---++--------/|  |||    |    |||  | |  ||| 
 || ||   |   ||     \------+-++-------------+--------------------+---+-++-----+++--+---------/ |    |    ||   ||         |  |||    |    |||  | |  ||| 
 || ||   |   ||            | ||             |   /----------------+---+-++---\ |||  |           |    |    ||   ||         |  |||    |    |||  | |  ||| 
 || ||   |   ||            \-++-------------+---+----------------/   | \+---+-+++--+-----------+----+----++---++---------+--/||    |    |||  | |  ||| 
 \+-++---+---/\--------------++-------------+---+--------------------/  |   | |||  |           |    |    ||   ||         |   ||    |    ||\--+-+--++/ 
  | \+---+-------------------++-------------+---+-----------------------+---+-+/|  |           |    |    ||   ||         |   ||    |    \+---+-+--+/  
  |  |   |                   ||             |   |                       \---+-+-+--+-----------+----+----++---++---------/   ||    |     |   | |  |   
  |  \---+-------------------++-------------+---+---------------------------+-+-+--/           |    |    \+---++-------------++----+-----+---/ |  |   
  |      |                   \+-------------+---+---------------------------+-+-+--------------+----+-----+---+/             |\----+-----/     |  |   
  \------+--------------------+-------------+---+---------------------------+-/ |              |    \-----+---+--------------+-----/           |  |   
         |                    |             |   |                           |   |              \----------+---+--------------+-----------------+--/   
         |                    \-------------+---+-<-------------------------+---+-------------------------/   \--------------+-----------------/      
         |                                  |   |                           |   |                                            |                        
         \----------------------------------/   \---------------------------/   \--------------------------------------------/                        
                                                                                                                                                      
`

    const track = []
    const trackLines = text.split(/[\r\n]+/)

    for (let y = 0; y < trackLines.length; y++) {
        let line = trackLines[y]
        track[y] = []

        for (let x = 0; x < line.length; x++) {
            track[y].push(line[x])
        }
    }
    return track
})()