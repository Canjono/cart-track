(function () {
    const track = window.track
    carts = getCarts(track)
    sortCarts()
    const drawer = window.drawer
    let currentCartIndex = 0
    let collisions = []

    drawer.init(track, carts)
    drawer.drawTrack()
    drawer.drawCarts(carts)
    let interval = setInterval(() => moveCart(), 5)

    function getCarts(track) {
        const carts = []
        const directions = { '>': 'right', 'v': 'down', '<': 'left', '^': 'up' }
        const trackUnderCarts = { '>': '-', 'v': '|', '<': '-', '^': '|' }
        let idCounter = 0

        for (let y = 0; y < track.length; y++) {
            for (let x = 0; x < track[y].length; x++) {
                let trackSymbol = track[y][x]

                if (!directions.hasOwnProperty(trackSymbol)) {
                    continue
                }

                let cart = Object.create(window.cart)
                cart.init(++idCounter, x, y, directions[trackSymbol])
                carts.push(cart)
                track[y][x] = trackUnderCarts[trackSymbol]
            }
        }

        return carts
    }

    function sortCarts() {
        carts.sort((current, next) => {
            if (current.x < next.x) {
                return -1
            }
            if (current.x > next.x) {
                return 1
            }
            if (current.y < next.y) {
                return -1
            }
            if (current.y > next.y) {
                return 1
            }
            return 0
        })
    }

    function moveCart() {
        currentCart = carts[currentCartIndex]
        drawer.drawTrackPosition(currentCart.x, currentCart.y)
        currentCart.move(track)

        const collideCart = checkCollision(currentCart)

        if (collideCart) {
            console.log(`Collisions between carts ${currentCart.id} and ${collideCart.id} on position ${currentCart.x},${currentCart.y - 1}`)
            collisions.push([currentCart, collideCart])
            carts = carts.filter(x => x.id != currentCart.id && x.id != collideCart.id)
            if (carts.length == 1) {
                const lastCart = carts[0]
                console.log(`Last cart ${lastCart.id} stopped on position ${lastCart.x},${lastCart.y - 1}`)
                clearInterval(interval)
                return
            }
        } else {
            drawer.drawCart(currentCart)
        }

        if (++currentCartIndex >= carts.length) {
            currentCartIndex = 0
            sortCarts()
        }
    }

    function checkCollision(cart) {
        for (let i = 0; i < carts.length; i++) {
            let checkCart = carts[i]

            if (checkCart.id != cart.id && checkCart.x == cart.x && checkCart.y == cart.y) {
                return checkCart
            }
        }

        return
    }
})()
