window.cart = (function () {
    const cart = {
        id: null,
        x: 0,
        y: 0,
        image: 'image file',
        direction: 'up',
        turnOrder: ['left', 'forward', 'right'],
        turnIndex: null,

        init: function (id, x, y, direction) {
            this.id = id
            this.x = x
            this.y = y
            this.direction = direction
            this.turnIndex = 0
        },

        move: function (track) {
            const trackUnderCart = track[this.y][this.x]

            switch (trackUnderCart) {
                case ('-'):
                case ('|'):
                    this.moveForward()
                    break
                case ('\\'):
                    this.turnAtBackslash()
                    break
                case ('/'):
                    this.turnAtSlash()
                    break
                case ('+'):
                    this.turnAtCrossroad()
                    break
            }
        },

        moveForward: function () {
            switch (this.direction) {
                case ('up'):
                    this.y--
                    break
                case ('right'):
                    this.x++
                    break
                case ('down'):
                    this.y++
                    break
                case ('left'):
                    this.x--
                    break
            }
        },

        turnLeft: function () {
            switch (this.direction) {
                case ('up'):
                    this.x--
                    this.direction = 'left'
                    break
                case ('right'):
                    this.y--
                    this.direction = 'up'
                    break
                case ('down'):
                    this.x++
                    this.direction = 'right'
                    break
                case ('left'):
                    this.y++
                    this.direction = 'down'
                    break
            }
        },

        turnRight: function () {
            switch (this.direction) {
                case ('up'):
                    this.x++
                    this.direction = 'right'
                    break
                case ('right'):
                    this.y++
                    this.direction = 'down'
                    break
                case ('down'):
                    this.x--
                    this.direction = 'left'
                    break
                case ('left'):
                    this.y--
                    this.direction = 'up'
                    break
            }
        },

        turnAtBackslash: function () {
            switch (this.direction) {
                case ('up'):
                case ('down'):
                    this.turnLeft()
                    break
                case ('right'):
                case ('left'):
                    this.turnRight()
                    break
            }
        },

        turnAtSlash: function () {
            switch (this.direction) {
                case ('up'):
                case ('down'):
                    this.turnRight()
                    break
                case ('right'):
                case ('left'):
                    this.turnLeft()
                    break
            }
        },

        turnAtCrossroad: function () {
            switch (this.turnOrder[this.turnIndex]) {
                case ('left'):
                    this.turnLeft()
                    break
                case ('forward'):
                    this.moveForward()
                    break
                case ('right'):
                    this.turnRight()
                    break
            }

            if (++this.turnIndex >= this.turnOrder.length) {
                this.turnIndex = 0
            }
        }
    }

    return cart
})()