window.drawer = (function () {
    const drawer = {
        ctx: null,
        canvas: null,
        pixelSize: 8,
        track: null,
        carts: null,
        cartSymbol: { right: '>', down: 'v', left: '<', up: '^' },

        init: function (track, carts) {
            this.track = track
            this.carts = carts
            this.canvas = document.getElementById('mainCanvas')
            this.canvas.width = track.length * this.pixelSize
            this.canvas.height = track[1].length * this.pixelSize

            this.ctx = this.canvas.getContext('2d')
            this.ctx.font = `${this.pixelSize}px Arial`
        },

        clearScreen: function () {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        },

        drawTrack: function () {
            this.ctx.strokeStyle = 'black'
            for (let y = 0; y < track.length; y++) {
                for (let x = 0; x < track[y].length; x++) {
                    this.ctx.strokeText(track[y][x], x * this.pixelSize, y * this.pixelSize)
                }
            }
        },

        drawTrackPosition: function (x, y) {
            this.ctx.strokeStyle = 'blue'
            this.ctx.clearRect(x * this.pixelSize, y * this.pixelSize - this.pixelSize, this.pixelSize, this.pixelSize)
            this.ctx.strokeText(this.track[y][x], x * this.pixelSize, y * this.pixelSize)
        },

        drawCarts: function () {
            this.ctx.strokeStyle = 'red'
            for (let i = 0; i < this.carts.length; i++) {
                let cart = this.carts[i]
                this.ctx.strokeText(this.cartSymbol[cart.direction], cart.x * this.pixelSize, cart.y * this.pixelSize)
            }
        },

        drawCart: function (cart) {
            this.ctx.strokeStyle = 'red'
            this.ctx.strokeText(this.cartSymbol[cart.direction], cart.x * this.pixelSize, cart.y * this.pixelSize)
        }
    }

    return drawer
})()